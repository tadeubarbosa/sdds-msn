# SDDMSN
### Saudade MSN Messenger

Se você, assim como eu, sente saudade do MSN Messenger este pequeno aplicativo foi feito para você!

** Isto **não** se trata de nenhum aplicativo criado pelo grupo <a href="http://msn.com" target="_blank">MSN</a>, mas sim de um antigo usuário que sente saudade do __MSN Messenger__.

Ainda em processo de criação.
